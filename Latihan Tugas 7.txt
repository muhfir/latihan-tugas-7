SOAL 1 (Buat Database)

CREATE DATABASE MYSHOP;

SOAL 2 (MEMBUAT TABEL)

Tabel Users
CREATE TABLE Users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

Tabel categories
CREATE TABLE Categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

Tabel Items
CREATE TABLE Items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(10) NOT null, stock int(10) NOT null, categories_id int(10), FOREIGN KEY (categories_id) REFERENCES categories(id) )


SOAL 3 (Memasukkan Data)

Insert User
INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123")

Insert Categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded")

Insert Items
INSERT INTO items(name, description, price, stock, categories_id) VALUES ("Sumsang b50", "HP keren dari merek Sumsang", 4000000, 100, 1), ("Uniklooh", "Baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "Jam tangan anak yang jujur banget", 2000000, 10, 1)

SOAL 4
Mendapatkan data seluruh user pada table users, KECUALI password
SELECT name, email from users

Menampilkan tabel itmes dengan price > 1 juta
SELECT * from items WHERE price > 1000000

Mengambil data item pada table items yang memiliki name serupa atau mirip
SELECT * from items WHERE description LIKE "%keren%"

Menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items
SELECT items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items INNER JOIN categories on items.categories_id = categories.id

SOAL 5
Mengubah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. 
UPDATE items SET price=2500000 WHERE id = 1